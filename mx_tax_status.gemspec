Gem::Specification.new do |s|
  s.name        = 'mx_tax_status'
  s.version     = '0.0.1'
  s.summary     = 'Retrieve Mexican Tax Situation information from SAT website'
  s.description = 'A simple gem to return the data from SAT website of mexican tax situation information'
  s.authors     = ['Gregorio Vazquez', 'Herman Moreno']
  s.email       = 'gregoriovazya@gmail.com'
  s.files       = ['lib/mx_tax_status.rb', 'lib/tax_document.rb', 'lib/path_constants.rb',
                   'lib/individual_entity_document.rb', 'lib/business_entity_document.rb']
  s.homepage    = 'https://rubygems.org/gems/mx_tax_status'
  s.license = 'MIT'

  s.add_runtime_dependency 'nokogiri', '~> 1.14'
  s.add_runtime_dependency 'open-uri', '~> 0.3.0'
end
