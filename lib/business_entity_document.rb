require_relative 'path_constants'
class BusinessEntityDocument
  def initialize(doc)
    @doc = doc
  end

  def profile
    profile = {}
    profile[:name] =
      "#{@doc.xpath(PathConstants::XPATH_B_PROFILE_SOCIAL_REASON).text} #{@doc.xpath(PathConstants::XPATH_B_PROFILE_CAPITAL_REGIME).text}"
    profile[:birthday] = @doc.xpath(PathConstants::XPATH_B_PROFILE_CREATION_AT).text
    profile[:operations_started_at] = @doc.xpath(PathConstants::XPATH_B_PROFILE_OPERATIONS_STARTED_AT).text
    profile[:status] = @doc.xpath(PathConstants::XPATH_B_PROFILE_STATUS).text
    profile[:status_updated_at] = @doc.xpath(PathConstants::XPATH_B_PROFILE_STATUS_UPDATED_AT).text
    profile
  end

  def location
    location = {}
    location[:state] = @doc.xpath(PathConstants::XPATH_B_LOCATION_STATE).text
    location[:delegation] = @doc.xpath(PathConstants::XPATH_B_LOCATION_DELEGATION).text
    location[:neighborhood] = @doc.xpath(PathConstants::XPATH_B_LOCATION_NEIGHBORHOOD).text
    location[:type_of_road] = @doc.xpath(PathConstants::XPATH_B_LOCATION_TYPE_OF_ROAD).text
    location[:street_name] = @doc.xpath(PathConstants::XPATH_B_LOCATION_STREET_NAME).text
    location[:outside_number] = @doc.xpath(PathConstants::XPATH_B_LOCATION_OUTSIDE_NUMBER).text
    location[:inside_number] = @doc.xpath(PathConstants::XPATH_B_LOCATION_INSIDE_NUMBER).text
    location[:cp] = @doc.xpath(PathConstants::XPATH_B_LOCATION_CP).text
    location[:email] = @doc.xpath(PathConstants::XPATH_B_LOCATION_EMAIL).text
    location[:al] = @doc.xpath(PathConstants::XPATH_B_LOCATION_AL).text
    location
  end

  def tax_characteristics
    tax_characteristics = {}
    tax_characteristics[:regime] = @doc.xpath(PathConstants::XPATH_B_TAX_CHARACTERISTICS_REGIME).text
    tax_characteristics[:date_of_registration] =
      @doc.xpath(PathConstants::XPATH_B_TAX_CHARACTERISTICS_DATE_OF_REGISTRATION).text
    tax_characteristics
  end
end
