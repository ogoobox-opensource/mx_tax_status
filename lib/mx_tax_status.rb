require 'nokogiri'
require 'open-uri'
require_relative 'tax_document'
require_relative 'path_constants'

class MXTaxStatus
  def self.fetch(idcif:, rfc:)
    doc = Nokogiri::HTML5(URI.open("#{PathConstants::URL}#{idcif}_#{rfc}"))

    entity = :business if rfc.length == 12

    tax_status = TaxDocument.new(doc, entity).generate

    proof_tax_situation = {
      profile: {},
      location: {},
      tax_characteristics: {}
    }

    proof_tax_situation[:profile] = tax_status.profile
    proof_tax_situation[:location] = tax_status.location
    proof_tax_situation[:tax_characteristics] = tax_status.tax_characteristics

    proof_tax_situation
  end
end
