require_relative 'business_entity_document'
require_relative 'individual_entity_document'

class TaxDocument
  def initialize(doc, entity = :individual)
    @doc = doc
    @entity = entity
  end

  def generate
    return BusinessEntityDocument.new(@doc) if @entity == :business

    IndividualEntityDocument.new(@doc)
  end
end
