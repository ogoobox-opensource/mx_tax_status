class PathConstants
  URL = 'https://siat.sat.gob.mx/app/qr/faces/pages/mobile/validadorqr.jsf?D1=10&D2=1&D3='.freeze

  # Persona fisica
  XPATH_I_PROFILE_CURP = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_I_PROFILE_NAME = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze
  XPATH_I_PROFILE_FIRST_SURNAME = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[3]/td[2]'.freeze
  XPATH_I_PROFILE_SECOND_SURNAME = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[4]/td[2]'.freeze
  XPATH_I_PROFILE_BIRTHDAY = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[5]/td[2]'.freeze
  XPATH_I_PROFILE_OPERATIONS_STARTED_AT = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[6]/td[2]'.freeze
  XPATH_I_PROFILE_STATUS = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[7]/td[2]'.freeze
  XPATH_I_PROFILE_STATUS_UPDATED_AT = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[8]/td[2]'.freeze

  XPATH_I_LOCATION_STATE = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_I_LOCATION_DELEGATION = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze
  XPATH_I_LOCATION_NEIGHBORHOOD = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[3]/td[2]'.freeze
  XPATH_I_LOCATION_TYPE_OF_ROAD = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[4]/td[2]'.freeze
  XPATH_I_LOCATION_STREET_NAME = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[5]/td[2]'.freeze
  XPATH_I_LOCATION_OUTSIDE_NUMBER = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[6]/td[2]'.freeze
  XPATH_I_LOCATION_INSIDE_NUMBER = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[7]/td[2]'.freeze
  XPATH_I_LOCATION_CP = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[8]/td[2]'.freeze
  XPATH_I_LOCATION_EMAIL = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[9]/td[2]'.freeze
  XPATH_I_LOCATION_AL = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[10]/td[2]'.freeze

  XPATH_I_TAX_CHARACTERISTICS_REGIME = '//*[@id="ubicacionForm:j_idt10:2:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_I_TAX_CHARACTERISTICS_DATE_OF_REGISTRATION = '//*[@id="ubicacionForm:j_idt10:2:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze

  # Personal moral
  XPATH_B_PROFILE_SOCIAL_REASON = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_B_PROFILE_CAPITAL_REGIME = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze
  XPATH_B_PROFILE_CREATION_AT = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[3]/td[2]'.freeze
  XPATH_B_PROFILE_OPERATIONS_STARTED_AT = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[4]/td[2]'.freeze
  XPATH_B_PROFILE_STATUS = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[5]/td[2]'.freeze
  XPATH_B_PROFILE_STATUS_UPDATED_AT = '//*[@id="ubicacionForm:j_idt10:0:j_idt11:j_idt15_data"]/tr[6]/td[2]'.freeze

  XPATH_B_LOCATION_STATE = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_B_LOCATION_DELEGATION = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze
  XPATH_B_LOCATION_NEIGHBORHOOD = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[3]/td[2]'.freeze
  XPATH_B_LOCATION_TYPE_OF_ROAD = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[4]/td[2]'.freeze
  XPATH_B_LOCATION_STREET_NAME = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[5]/td[2]'.freeze
  XPATH_B_LOCATION_OUTSIDE_NUMBER = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[6]/td[2]'.freeze
  XPATH_B_LOCATION_INSIDE_NUMBER = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[7]/td[2]'.freeze
  XPATH_B_LOCATION_CP = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[8]/td[2]'.freeze
  XPATH_B_LOCATION_EMAIL = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[9]/td[2]'.freeze
  XPATH_B_LOCATION_AL = '//*[@id="ubicacionForm:j_idt10:1:j_idt11:j_idt15_data"]/tr[10]/td[2]'.freeze

  XPATH_B_TAX_CHARACTERISTICS_REGIME = '//*[@id="ubicacionForm:j_idt10:2:j_idt11:j_idt15_data"]/tr[1]/td[2]'.freeze
  XPATH_B_TAX_CHARACTERISTICS_DATE_OF_REGISTRATION = '//*[@id="ubicacionForm:j_idt10:2:j_idt11:j_idt15_data"]/tr[2]/td[2]'.freeze
end
