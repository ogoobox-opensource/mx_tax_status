# MX Tax Status Ruby Gem

### Summary
Retrieve Mexican Tax Situation information from SAT website

### Description
A simple gem to return the data from SAT website of mexican tax situation information

### Dependency gems

- nokogiri
- open-uri

### Required fields to use it

- `id_CIF`: Cédula de Identificación Fiscal
- `RFC`: Registro Federal de Contribuyentes